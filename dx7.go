package dx7 // import "jacobvosmaer.nl/go/dx7"

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strings"
)

const (
	MinMessageLength = 4
	EOSysex          = 0xf7
)

func IsMk1(msg []byte) bool {
	if len(msg) < 4 {
		return false
	}

	if !bytes.Equal(msg[0:3], []byte{0xf0, 0x43, 0x00}) {
		return false
	}

	return bytes.Contains([]byte{0x00, 0x05, 0x06, 0x09}, msg[3:4])
}

func DataLen(msb, lsb byte) int {
	return (int(msb) << 7) + int(lsb)
}

func Checksum(data []byte) byte {
	var sum byte
	for _, b := range data {
		sum = sum + b
	}

	return (128 - sum) & 0x7f
}

func IsPackedVoices(msg []byte) bool {
	return len(msg) == 4104 && bytes.Equal([]byte{0xf0, 0x43, 0x00, 0x09, 0x20, 0x00}, msg[0:6])
}

// SysexSplitFunc can be used with bufio.Scanner.
func SysexSplitFunc(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if len(data) < MinMessageLength {
		if atEOF && len(data) > 0 {
			return 0, nil, fmt.Errorf("unexpected trailing data in sysex: %x", data)
		}
		return 0, nil, nil
	}

	if h := data[:2]; h[0] != 0xf0 || h[1] != 0x43 {
		return 0, nil, fmt.Errorf("invalid sysex message start %x", h)
	}

	if n := bytes.Index(data, []byte{EOSysex}); n >= 0 {
		return n + 1, data[:n+1], nil
	}

	return 0, nil, nil
}

func SplitMessages(r io.Reader) ([][]byte, error) {
	in := bufio.NewReader(r)
	var msgs [][]byte
	var err error
	for {
		var msg []byte
		msg, err = in.ReadBytes(EOSysex)
		l := len(msg)
		if err != nil || l == 0 {
			break
		}

		if l < MinMessageLength {
			return nil, fmt.Errorf("message too short: % x", msg)
		}

		if h := msg[:2]; !bytes.Equal(h, []byte{0xf0, 0x43}) {
			return nil, fmt.Errorf("invalid sysex message start %x", h)
		}

		msgs = append(msgs, msg)
	}

	if err == io.EOF {
		err = nil
	}
	return msgs, err
}

func Type(_msg []byte) (string, bool) {
	msg := string(_msg)
	if !strings.HasPrefix(msg, "\xf0\x43") {
		return "", false
	}

	switch {
	case msg[2:4] == "\x00\x00":
		return "voice edit buffer", true
	case msg[2:4] == "\x00\x05":
		return "supplement edit buffer", true
	case msg[2:4] == "\x00\x06":
		return "packed 32 supplement", true
	case msg[2:4] == "\x00\x09":
		return "packed 32 voice", true
	case msg[2:4] == "\x00\x7e":
		if len(msg) < 18 {
			return "", false
		}
		return msg[6:16], true
	case msg[2:5] == "\x10\x19\x4d":
		return "midi receive block", true
	}

	return "", false
}

func Data(msg []byte) ([]byte, error) {
	if !IsMk1(msg) {
		return nil, fmt.Errorf("unsupported")
	}

	const metaLen = 8 // 6 header, 2 trailer
	if len(msg) < metaLen {
		return nil, fmt.Errorf("incomplete dx7 message: %x", msg)
	}

	dataLen := DataLen(msg[4], msg[5])
	if n := dataLen + metaLen; len(msg) != n {
		return nil, fmt.Errorf("expected msg length %d, got %d", n, len(msg))
	}

	data := msg[6 : 6+dataLen]
	if expected, actual := msg[len(msg)-2], Checksum(data); expected != actual {
		return nil, fmt.Errorf("expected checksum %x, got %x", expected, actual)
	}

	return data, nil
}
