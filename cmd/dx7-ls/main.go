package main

import (
	"bufio"
	"fmt"
	"os"

	"jacobvosmaer.nl/go/dx7"
)

const (
	progName = "dx7-ls"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "usage: %s FILE\n", progName)
		os.Exit(1)
	}

	if err := _main(os.Args[1]); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}

func _main(sysexPath string) error {
	f, err := os.Open(sysexPath)
	if err != nil {
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Split(dx7.SysexSplitFunc)

	for scanner.Scan() {
		msg := scanner.Bytes()
		l := len(msg)
		contents := fmt.Sprintf("% x ... % x", msg[:dx7.MinMessageLength], msg[l-2:l])
		if l < 10 {
			contents = fmt.Sprintf("% x", msg)
		}

		fmt.Printf("%s (%d bytes)\n", contents, l)

		t, ok := dx7.Type(msg)
		if !ok {
			continue
		}

		fmt.Printf("  %s\n", t)

		if t == "packed 32 voice" {
			data, err := dx7.Data(msg)
			if err != nil {
				return err
			}

			for i := 0; i < 32; i++ {
				v := data[i*128 : (i+1)*128]
				name := v[118:128]
				alg := v[110] & 31
				fmt.Printf("    %2d) %s	(alg %d)\n", i+1, name, alg)
			}
		}
	}

	return scanner.Err()
}
