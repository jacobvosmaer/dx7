package main

import (
	"bufio"
	"fmt"
	"io"
	"os"

	"jacobvosmaer.nl/go/dx7"
)

const progname = "dx7-has-fixed-pitch"

func main() {
	if err := _main(); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v", err)
		os.Exit(1)
	}
}

func _main() error {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		ok, err := hasFixedPitch(scanner.Text())
		if err != nil {
			return err
		}

		if ok {
			fmt.Println(scanner.Text())
		}
	}

	return scanner.Err()
}

func hasFixedPitch(d7v string) (bool, error) {
	f, err := os.Open(d7v)
	if err != nil {
		return false, err
	}
	defer f.Close()

	voice := make([]byte, len(dx7.InitVoice))
	if _, err := io.ReadFull(f, voice); err != nil {
		return false, err
	}

	for i := 0; i < 6; i++ {
		if voice[15+i*17]&1 > 0 {
			return true, nil
		}
	}

	return false, nil
}
