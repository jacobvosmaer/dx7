package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"jacobvosmaer.nl/go/dx7"
)

const (
	dataSize  = 4096
	voiceSize = 128
)

func main() {
	if err := _main(); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}

func _main() error {
	data, err := ioutil.ReadAll(io.LimitReader(os.Stdin, dataSize+1))
	if err != nil {
		return err
	}

	if len(data) > dataSize {
		return fmt.Errorf("read more than %d bytes from stdin", dataSize)
	}

	if l := len(data); l%voiceSize != 0 {
		return fmt.Errorf("number of input bytes %d not divisible by voice size %d", l, voiceSize)
	}

	for len(data) < dataSize {
		data = append(data, dx7.InitVoice[:]...)
	}

	w := bufio.NewWriter(os.Stdout)
	fmt.Fprintf(w, "\xf0\x43\x00\x09\x20\x00")
	w.Write(data)
	w.Write([]byte{dx7.Checksum(data), dx7.EOSysex})

	if err := w.Flush(); err != nil {
		return err
	}

	return nil
}
