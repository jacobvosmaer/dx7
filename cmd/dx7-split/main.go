package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"jacobvosmaer.nl/go/dx7"
)

const (
	progName = "dx7-split"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "usage: %s FILE\n", progName)
		os.Exit(1)
	}

	if err := _main(os.Args[1]); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}

func _main(sysexPath string) error {
	f, err := os.Open(sysexPath)
	if err != nil {
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Split(dx7.SysexSplitFunc)

	count := 0
	for scanner.Scan() {
		msg := scanner.Bytes()
		t, ok := dx7.Type(msg)
		if !(ok && t == "packed 32 voice") {
			continue
		}

		data, err := dx7.Data(msg)
		if err != nil {
			return err
		}

		for len(data) > 0 {
			var v []byte
			v, data = data[:128], data[128:]
			voiceName := string(v[118:128])
			fileName := fmt.Sprintf("%03d-%s.d7v", count+1, strings.Replace(voiceName, "/", "_", -1))
			fmt.Println(fileName)
			if err := ioutil.WriteFile(fileName, v, 0644); err != nil {
				return err
			}
			count++
		}
	}

	return scanner.Err()
}
